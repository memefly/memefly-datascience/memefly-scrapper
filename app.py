import platform
from selenium import webdriver
import os, sys
import time
from selenium.webdriver.common.action_chains import ActionChains
import json
import time
import boto3
import requests
from decouple import config

DRIVER_FOLDER = 'driver'
WINDOWS_FOLDER = 'chromedriver_win32'
MAC_FOLDER = 'chromedriver_mac64'
LINUX_FOLDER = 'chromedriver_linux64'
MEME_NAMES = 'memefly_meme_names.txt'
SAMPLE_MEME_NAMES = 'sample_data.txt'
JSON_FILE_DUMP_NAME = 'memefly_data.json'


class WebDriverNotFoundException(Exception):
    pass


def main():

    full_path_ = 'No Path Found'
    if platform.system().lower().startswith('window'):
        print(f'[INFO] Using Windows WebDriver')
        full_path_ = os.path.join(DRIVER_FOLDER, WINDOWS_FOLDER, 'chromedriver.exe')
    elif platform.system().lower().startswith('darwin'):
        full_path_ = os.path.join(DRIVER_FOLDER, MAC_FOLDER, 'chromedriver')
        print(f'[INFO] Using MacOS Darwin WebDriver')
    else:
        if full_path_ == 'No Path Found':
            raise WebDriverNotFoundException
        
    print(f'[INFO] Path of Webdriver: {full_path_}')
    
    driver = webdriver.Chrome(full_path_)
    driver.maximize_window()
    
    meme_names = []
    bbox_data = []
    
    print(f'[INFO] Loading {MEME_NAMES} into list')
    with open(MEME_NAMES) as fobj:
        meme_names = [line.rstrip() for line in fobj]
    
    s3 = boto3.client(
        's3',
        aws_access_key_id=config('AWS_ACCESS_KEY'),
        aws_secret_access_key=config('AWS_SECRET_ACCESS_KEY')
    )
    
    BASE_URL = 'https://imgflip.com/memegenerator/'
    BUCKET_NAME = config('S3_BUCKET_NAME')
    S3_IMAGE_FOLDER = config('S3_FOLDER_NAME') 
    REGION = s3.get_bucket_location(Bucket=BUCKET_NAME)['LocationConstraint']
    S3_BASE_URL = f'https://{BUCKET_NAME}.s3.{REGION}.amazonaws.com'
    print(f'[INFO]  --------- AWS ----------')
    print(f'''
            BUCKET_NAME = {BUCKET_NAME}
            S3_IMAGE_FOLDER = {S3_IMAGE_FOLDER}
            REGION  = {REGION }
            S3_BASE_URL = {S3_BASE_URL}
            ''')
    print(f'[INFO] Setting Imgflip Base URL = {BASE_URL}')    
    
    for meme_name in meme_names:
        url = BASE_URL + meme_name
        # Wait 20 seconds for page to load
        timeout = 1
        time.sleep(timeout)
        print(f'[INFO] Fetching URL in Chrome: {url}')
        try:
            driver.get(url)
        except Exception as e:
            print(e)
            
        print(f'[INFO] URL successfully opened in Chrome')
        try:
            # Fetch base meme bounding box and imgflip url
            canvas_prev_ele = driver.find_elements_by_css_selector('#mm-preview-outer')
            hover = ActionChains(driver).move_to_element(canvas_prev_ele[0])
            hover.perform()
            
            mm_preview_ele = driver.find_elements_by_css_selector('#mm-preview-outer > div.mm-preview')
            bbox_eles = mm_preview_ele[0].find_elements_by_class_name('drag-box')
            meme_bbox = {}
            meme_name = meme_name.lower()
            meme_bbox["meme_name"] = meme_name
            meme_bbox["meme_bounding_box"] = [bbox_ele.get_attribute('style') for bbox_ele in bbox_eles] 
            
            print(f'[INFO] Bounding Box for {meme_name} parsed and saved successfully')
            '#mm-preview-outer > div.mm-preview > img'
            meme_url = mm_preview_ele[0].find_element_by_tag_name('img').get_attribute('src')
            
            # Fetch imgflip image from url
            headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Cafari/537.36'} 
            print(f'[INFO] Requesting base meme pic from Imgflip URL: {meme_url}')
            pic = requests.get(meme_url, headers=headers)
            
            # Upload  image to S3 bucket
            pic_filename = meme_name + '.jpg'
            folder_path = '/'.join([S3_IMAGE_FOLDER, pic_filename])
            print(f'[INFO] Saving to S3 folder path: {folder_path}')
            s3.put_object(Body = pic.content, Bucket = BUCKET_NAME, Key = folder_path)
                        
            # Save image s3 link
            s3_url = S3_BASE_URL + '/' + folder_path
            print(f'[INFO] S3 image url: {s3_url}')
            meme_bbox["meme_url"] = s3_url
            
            # Now append the dictonary
            bbox_data.append(meme_bbox)
            
            
        except Exception as e:
            print(e)

    driver.close()
    with open(JSON_FILE_DUMP_NAME, 'w') as file_object: 
        json.dump(bbox_data, file_object, indent=4)
    print(f'[INFO] JSON file {JSON_FILE_DUMP_NAME} saved')
    
if __name__ == "__main__":
    main()
